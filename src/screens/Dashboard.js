import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  StyleSheet,
  FlatList,
  TextInput,
  Keyboard,
} from "react-native";
import { Card } from "react-native-card-mb";
import { Addition, Substraction } from "../reusableComponents";
import { isEmptyOrNull } from "../utils/Metrics";

var mathExpressions = require("react-native-math-expressions-mb");

const Dashboard = () => {
  const [number, setNumber] = useState();
  const [result, setResult] = useState();
  const [error, setError] = useState("");

  useEffect(() => {
    //Handled through local functions
    // console.log("Addition: ", Addition(20, 20));
    // console.log("Substraction: ", Substraction(20, 10));

    //Handled through SDK functions
    console.log("addNumbers: ", mathExpressions.addNumbers(10, 12));
    console.log("subNumbers: ", mathExpressions.subNumbers(50, 20));
  }, []);

  const handleAddition = () => {
    if (isEmptyOrNull(number)) {
      console.log("Please enter numbers");
      setError("Please enter numbers");
      return;
    }
    Keyboard.dismiss();
    setError("");
    let resultValue = mathExpressions.addNumbers(number);
    setResult("Result: " + resultValue);
  };

  const handleSubstracion = () => {
    if (isEmptyOrNull(number)) {
      console.log("Please enter numbers");
      setError("Please enter numbers");
      return;
    }
    Keyboard.dismiss();
    setError("");
    let resultValue = mathExpressions.subNumbers(number);
    setResult("Result: " + resultValue);
  };

  return (
    <>
      <View style={{ flex: 1 }}>
        <View style={{ marginTop: 30 }}>
          <TextInput
            placeholder="Enter numbers"
            value={number}
            onChangeText={(text) => setNumber(text)}
            style={styles.textFieldStyle}
            keyboardType="numbers-and-punctuation"
            maxLength={10}
          />
          <Text style={styles.infoStyle}>Enter comma separated numbers</Text>
        </View>

        <Text style={styles.resultStyle}>
          {isEmptyOrNull(error) ? (isEmptyOrNull(result) ? "" : result) : error}
        </Text>

        <Card
          disabled={false}
          onPress={() => handleAddition()}
          children={
            <View>
              <Text>ADD</Text>
            </View>
          }
          cardContainterStyle={styles.buttonContainer}
        />
        <Card
          disabled={false}
          onPress={() => handleSubstracion()}
          children={
            <View>
              <Text>SUB</Text>
            </View>
          }
          cardContainterStyle={styles.buttonContainer}
        />
        <Card
          disabled={true}
          // onPress={() => handleSubstracion()}
          children={
            <View style={styles.exampleContainer}>
              <Text style={[styles.exampleStyle, { fontWeight: "bold" }]}>
                Examples:-
              </Text>
              <Text style={styles.exampleStyle}>Add Numbers Expression:</Text>
              <Text style={[styles.exampleStyle, { fontStyle: "italic" }]}>
                {`mathExpressions.addNumbers(10, 12)\nResult: ${mathExpressions.addNumbers(
                  10,
                  12
                )}`}
              </Text>
              <Text style={[styles.exampleStyle, { marginTop: 15 }]}>
                Sub Numbers Expression:
              </Text>
              <Text style={[styles.exampleStyle, { fontStyle: "italic" }]}>
                {`mathExpressions.subNumbers(50, 20)\nResult: ${mathExpressions.subNumbers(
                  50,
                  20
                )}`}
              </Text>
            </View>
          }
          cardContainterStyle={{ borderRadius: 10 }}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  textFieldStyle: {
    marginHorizontal: 20,
    marginBottom: 5,
    padding: 10,
    height: 44,
    borderColor: "gray",
    borderWidth: 1,
    borderRadius: 10,
  },
  resultStyle: {
    alignSelf: "center",
    padding: 10,
    height: 44,
    fontSize: 18,
    fontWeight: "bold",
  },
  buttonContainer: {
    height: 50,
    borderRadius: 10,
  },
  infoStyle: {
    marginHorizontal: 20,
    fontSize: 12,
    fontStyle: "italic",
  },
  exampleContainer: {
    width: "100%",
    padding: 10,
  },
  exampleStyle: {
    fontSize: 15,
    marginBottom: 8,
  },
});

export default Dashboard;
